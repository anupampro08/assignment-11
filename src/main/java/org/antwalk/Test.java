package org.antwalk;

import java.util.List;
import java.util.Scanner;

import org.antwalk.Dao.PhoneDao;
import org.antwalk.model.Phone;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test
{

	public static void main(String[] args) 
	{
		ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");
		PhoneDao ph=(PhoneDao)context.getBean("PhoneDaoImpl");
		String brand,res;
		int price;
		
		
		
		do {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter phone name: ");
		brand = in.nextLine();
		
		System.out.println("Enter price: ");
		price = in.nextInt();
		
		System.out.println("Adding Records");
		ph.create(brand,price);
		
		System.out.println("Want to add new records yes/no:");
		res = in.next();
		
//		in.close();
		}while(res.equals("yes") || res.equals("YES"));
		
		
		System.out.println("\nListing the phones:");
		List<Phone>phLi=ph.listPhones();
		for(Phone p:phLi)
		{
			System.out.println("\nId:"+p.getId());
			System.out.println("Brand:"+p.getBrand());
			System.out.println("Price:"+p.getPrice());
		}
	}

}